#proc page
  pagesize 10 8

#proc areadef
  rectangle: 1 1 7 4
  xrange: -1 512
  yrange:  0 1.191458e+03
  xaxis.stubs: inc
  yaxis.stubs: inc
  xaxis.stubrange: 0 512
  xaxis.stubvert: yes
  xaxis.label: MPI rank
  xaxis.labeldistance: 0.5
  yaxis.label: time in seconds

#proc getdata
   file: xcompact3d_512_racklett.1650207530.597644.ipm.xml_ipm_8262040/pl/task_data.txt

#proc bars
   lenfield: 4
   locfield: 2
   color: red
   legendlabel: MPI_Alltoallv
   barwidth: 0.013645
   outline: no
   #saveas A

#proc bars
   #clone: A
   lenfield: 5
   color: green
   legendlabel: MPI_Allreduce
   stackfields: *

#proc bars
   #clone: A
   lenfield: 6
   color: blue
   legendlabel: MPI_Reduce
   stackfields: *

#proc bars
   #clone: A
   lenfield: 7
   color: yellow
   legendlabel: MPI_Waitall
   stackfields: *

#proc bars
   #clone: A
   lenfield: 8
   color: purple
   legendlabel: MPI_Issend
   stackfields: *

#proc bars
   #clone: A
   lenfield: 9
   color: coral
   legendlabel: MPI_Irecv
   stackfields: *

#proc bars
   #clone: A
   lenfield: 10
   color: orange
   legendlabel: MPI_Comm_rank
   stackfields: *

#proc bars
   #clone: A
   lenfield: 11
   color: darkblue
   legendlabel: MPI_Comm_size
   stackfields: *

#proc bars
   #clone: A
   lenfield: 12
   color: limegreen
   legendlabel: MPI_Init
   stackfields: *

#proc bars
   #clone: A
   lenfield: 13
   color: skyblue
   legendlabel: MPI_Finalize
   stackfields: *

#proc legend
  location: max+0.5 max
  seglen: 0.3
