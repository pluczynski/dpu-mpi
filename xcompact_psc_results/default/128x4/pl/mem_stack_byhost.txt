#proc page
  pagesize 10 8

#proc areadef
  rectangle: 1 1 8 4
  xrange: 0 5
  yrange:  0 56.113792
  yaxis.stubs: inc
  xaxis.stubvert: yes
  yaxis.label: GBytes
  xaxis.stubs: text
        r259.ib.bridges2.psc.edu
        r292.ib.bridges2.psc.edu
        r295.ib.bridges2.psc.edu
        r307.ib.bridges2.psc.edu

#proc getdata
  data:
0 56.113792
1 56.113792
2 56.113792
3 56.113792

 #proc bars
  lenfield: 2
  color: green
  barwidth: 1.200000
  outline: no
  #saveas A

