#proc page
  pagesize 10 8

#proc areadef
    rectangle: 0 0 2.5 2
    xrange: 0 1
    yrange: 0 1

#proc getdata
data:
MPI_Alltoallv 71.646791 red MPI_Alltoallv
MPI_Allreduce 25.386018 green MPI_Allreduce
MPI_Waitall 1.411555 blue MPI_Waitall
MPI_Reduce 1.207008 yellow MPI_Reduce
MPI_Issend 0.338497 purple MPI_Issend
MPI_Irecv 0.010008 coral MPI_Irecv
MPI_Comm_rank 0.000086 orange MPI_Comm_rank
MPI_Comm_size 0.000036 darkblue MPI_Comm_size
MPI_Init 0.000000 limegreen MPI_Init
MPI_Finalize 0.000000 skyblue MPI_Finalize

#proc pie
 firstslice: 0
 datafield: 2
 labelfield: 1
 exactcolorfield: 3
 center: 0.5(s) 0.6(s)
 radius: 0.4(s)

#proc legend
 location: 1.1(s) 1.2(s)
