#proc getdata:
    file: xcompact3d_512_racklett.1650206691.389127.ipm.xml_ipm_8261822/pl/func_data.txt

#proc page
  pagesize 10 8

#proc areadef
rectangle: 1 1 8 4
xautorange: 8 16777216
yautorange: 0 27115040
yaxis.stubs: inc
xscaletype: log
yscaletype: log
yaxis.label: # calls

#proc xaxis
  label: Buffer size (bytes)
  selflocatingstubs: text
	1          1
	4          4
	16         16
	64         64
	256        256
	1024       1KB
	4096       4KB
	16384      16KB
	65536      64KB
	262144     256KB
	1048576    1MB
	4194304    4MB
	16777216   16MB
	67108864   64MB
	268435456  256MB
	1073741824 1GB

#proc lineplot
xfield: 2
yfield: 3
sort: yes
select: @@1 = MPI_Issend
linedetails: color=red
legendlabel: MPI_Issend
pointsymbol: shape=circle linecolor=black radius=0.03 fillcolor=red

#proc lineplot
xfield: 2
yfield: 3
sort: yes
select: @@1 = MPI_Irecv
linedetails: color=green
legendlabel: MPI_Irecv
pointsymbol: shape=circle linecolor=black radius=0.03 fillcolor=green

#proc lineplot
xfield: 2
yfield: 3
sort: yes
select: @@1 = MPI_Reduce
linedetails: color=blue
legendlabel: MPI_Reduce
pointsymbol: shape=circle linecolor=black radius=0.03 fillcolor=blue

#proc lineplot
xfield: 2
yfield: 3
sort: yes
select: @@1 = MPI_Allreduce
linedetails: color=yellow
legendlabel: MPI_Allreduce
pointsymbol: shape=circle linecolor=black radius=0.03 fillcolor=yellow

#proc lineplot
xfield: 2
yfield: 3
sort: yes
select: @@1 = MPI_Alltoallv
linedetails: color=purple
legendlabel: MPI_Alltoallv
pointsymbol: shape=circle linecolor=black radius=0.03 fillcolor=purple

#proc legend
  location: max max

